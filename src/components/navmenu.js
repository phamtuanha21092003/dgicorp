"use client"
import { Link } from "@/navigation"
import { useTranslations } from "next-intl"
import Image from "next/image"
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight"
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown"

import React, { useState } from "react"

export default function NavMenu({ open, setOpen, switchLanguage }) {
    const [openSv, setOpenSv] = useState(open)
    const t = useTranslations("Index")
    const t_btn = useTranslations("btn")
    return (
        <div className="flex flex-col items-start gap-4 text-lg">
            <Link href="/" onClick={() => setOpen(!open)}>
                {t("menu_home")}
            </Link>
            <hr className="bg-white w-full" />
            <div className="w-full justify-center">
                <div
                    onClick={() => {
                        setOpenSv(!openSv)
                    }}
                    className="flex justify-between"
                >
                    <div>{t("menu_services")}</div>
                    {openSv ? (
                        <KeyboardArrowDownIcon />
                    ) : (
                        <KeyboardArrowRightIcon />
                    )}
                </div>
                {openSv && (
                    <ul className="ml-4">
                        <li>
                            <Link
                                href="/service_1"
                                onClick={() => setOpen(!open)}
                            >
                                {t_btn("sv_1")}
                            </Link>
                        </li>
                        <li>
                            <Link
                                href="/service_2"
                                onClick={() => setOpen(!open)}
                            >
                                {t_btn("sv_2")}
                            </Link>
                        </li>
                        <li>
                            <Link
                                href="/service_3"
                                onClick={() => setOpen(!open)}
                            >
                                {t_btn("sv_3")}
                            </Link>
                        </li>
                        <li>
                            <Link
                                href="/service_4"
                                onClick={() => setOpen(!open)}
                            >
                                {t_btn("sv_4")}
                            </Link>
                        </li>
                    </ul>
                )}
            </div>
            <hr className="bg-white w-full" />
            <Link href="/about" onClick={() => setOpen(!open)}>
                {t("menu_about")}
            </Link>
            <hr className="bg-white w-full" />
            <Link href="/contact" onClick={() => setOpen(!open)}>
                {t("button_contact")}
            </Link>
            <hr className="bg-white w-full" />
            <div className=" flex gap-2 cursor-pointer items-center">
                <div onClick={() => switchLanguage("vie")}>
                    <Image
                        src="/vie.png"
                        width={20}
                        height={18}
                        alt="vietnam"
                    />
                </div>
                <div onClick={() => switchLanguage("en")}>
                    <Image
                        src="/america.png"
                        width={20}
                        height={18}
                        alt="america"
                    />
                </div>
            </div>
            <hr className="bg-white w-full" />
        </div>
    )
}
