"use client"
import React from "react"
import ArrowBackIcon from "@mui/icons-material/ArrowBack"
import { useTranslations } from "next-intl"
import Image from "next/image"
import { Link } from "@/navigation"

export default function Services({ title, children }) {
    const t = useTranslations("btn")

    return (
        <>
            <div className="flex  items-start flex-col gap-1 py-4 ">
                <p className="font-bold text-2xl w-full text-center">{title}</p>

                {children}
            </div>
            <div className="flex justify-end">
                <Image width={120} height={120} src="/dg_logo.png" alt="logo" />
            </div>
        </>
    )
}
