import React from "react"
import StarIcon from "@mui/icons-material/Star"
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye"
import Image from "next/image"
import { useTranslations } from "next-intl"

export default function Article() {
    const t = useTranslations("article")
    return (
        <div className="flex flex-col  md:grid md:grid-cols-12">
            <div className="w-full h-[300px] md:h-auto md:col-span-6 bg-[url('/1.jpg')] bg-cover bg-no-repeat bg-center"></div>

            <div className="w-full md:col-span-6 md:items-center p-4  ">
                <div className="flex flex-col md:flex-row gap-2">
                    <div className="w-full md:w-1/2 p-2">
                        <div className="flex items-center gap-2">
                            <div className="flex-shrink-0">
                                <div className="p-[2px] rounded-full bg-gradient-to-r from-yellow-200 to-yellow-400">
                                    <div className="border-4 border-transparent bg-clip-border bg-white p-1 rounded-full">
                                        <RemoveRedEyeIcon
                                            className="text-yellow-300"
                                            fontSize="small"
                                        />
                                    </div>
                                </div>
                            </div>
                            <p className="font-bold"> {t("vision.title")}</p>
                        </div>
                        <p className="indent-12">{t("vision.content")}</p>
                    </div>
                    <div className="w-full md:w-1/2 p-2">
                        <div className="flex items-center gap-2">
                            <div className="flex-shrink-0">
                                <div className="p-[2px] rounded-full bg-gradient-to-r from-yellow-200 to-yellow-400">
                                    <div className="border-4 border-transparent bg-clip-border bg-white p-1 rounded-full">
                                        <RemoveRedEyeIcon
                                            className="text-yellow-300"
                                            fontSize="small"
                                        />
                                    </div>
                                </div>
                            </div>
                            <p className="font-bold">{t("mission.title")}</p>
                        </div>
                        <p className="indent-12">{t("mission.content")}</p>
                    </div>
                </div>
                <div className="flex flex-col md:flex-col md:items-center  gap-4">
                    <div className="flex-shrink-0 mx-auto md:mx-0">
                        <div className="p-1 rounded-full bg-gradient-to-r from-yellow-200 to-yellow-400">
                            <div className="border-4 border-transparent bg-clip-border bg-white p-6 rounded-full">
                                <Image
                                    src="/dg_logo.png"
                                    width={124}
                                    height={124}
                                    alt="logo_dg"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="flex flex-col md:flex-row md:gap-2">
                        <ul className="leading-loose">
                            <li className="flex gap-2">
                                <StarIcon color="info" />
                                {t("ul.1")}
                            </li>
                            <li className="flex gap-2">
                                <StarIcon color="info" />
                                {t("ul.2")}
                            </li>
                            <li className="flex gap-2">
                                <StarIcon color="info" /> {t("ul.3")}
                            </li>
                        </ul>
                        <ul className="leading-loose">
                            <li className="flex gap-2">
                                <StarIcon color="info" /> {t("ul.4")}
                            </li>
                            <li className="flex gap-2">
                                <StarIcon color="info" /> {t("ul.5")}
                            </li>
                            <li className="flex gap-2">
                                <StarIcon color="info" /> {t("ul.6")}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
