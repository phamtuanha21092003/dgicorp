import { Link } from "@/navigation"
import { useTranslations } from "next-intl"
import React, { useState } from "react"

export default function Dropdown() {
    const t = useTranslations("Index")
    const t_btn = useTranslations("btn")
    const [open, setOpen] = useState(false)

    const onClose = () => {
        setOpen(!open)
    }

    const handleBlur = (e) => {
        // Kiểm tra nếu click ra bên ngoài dropdown thì đóng dropdown
        if (!e.currentTarget.contains(e.relatedTarget)) {
            setOpen(false)
        }
    }

    return (
        <div
            className="relative inline-block text-left"
            onBlur={handleBlur}
            tabIndex={0}
        >
            <button onClick={onClose}>{t("menu_services")}</button>
            {open && (
                <div
                    className="absolute w-56 left-[-65px] z-10 mt-11 inline-block origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
                    role="menu"
                    aria-orientation="vertical"
                    aria-labelledby="menu-button"
                    tabIndex="-1"
                >
                    <div className="p-1" role="none">
                        <Link
                            onClick={onClose}
                            href="/service_1"
                            className="block px-4 py-2 text-sm text-gray-700 rounded hover:bg-black hover:text-white"
                            role="menuitem"
                            tabIndex="-1"
                            id="menu-item-0"
                        >
                            {t_btn("sv_1")}
                        </Link>
                        <Link
                            onClick={onClose}
                            href="/service_2"
                            className="block px-4 py-2 text-sm text-gray-700 rounded hover:bg-black hover:text-white"
                            role="menuitem"
                            tabIndex="-1"
                            id="menu-item-1"
                        >
                            {t_btn("sv_2")}
                        </Link>
                        <Link
                            onClick={onClose}
                            href="/service_3"
                            className="block px-4 py-2 text-sm text-gray-700 rounded hover:bg-black hover:text-white"
                            role="menuitem"
                            tabIndex="-1"
                            id="menu-item-2"
                        >
                            {t_btn("sv_3")}
                        </Link>
                        <Link
                            onClick={onClose}
                            href="/service_4"
                            className="block px-4 py-2 text-sm text-gray-700 rounded hover:bg-black hover:text-white"
                            role="menuitem"
                            tabIndex="-1"
                            id="menu-item-3"
                        >
                            {t_btn("sv_4")}
                        </Link>
                    </div>
                </div>
            )}
        </div>
    )
}
