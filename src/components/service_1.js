import Services from "@/components/services"
import { useTranslations } from "next-intl"
import React from "react"

export default function Service_1() {
    const t = useTranslations("Services")
    return (
        <>
            <Services title={t("services1.title")}>
                <div className="flex flex-col gap-3 px-2 ld:px-6">
                    <p class="indent-8 text-center">
                        {t("services1.introduce")}
                    </p>
                    <ul className="list-decimal leading-loose px-2">
                        <li>
                            <h1 className="font-bold text-xl">
                                {t("services1.contents.0.title")}
                            </h1>
                            <ul class="list-disc">
                                <li>{t("services1.contents.0.content.0")}</li>
                                <li>{t("services1.contents.1.content.1")}</li>
                                <li>{t("services1.contents.2.content.2")}</li>
                            </ul>
                        </li>
                        <li>
                            <h1 className="font-bold text-xl">
                                {t("services1.contents.1.title")}
                            </h1>
                            <ul class="list-disc">
                                <li>{t("services1.contents.1.content.0")}</li>
                                <li>{t("services1.contents.1.content.1")}</li>
                                <li>{t("services1.contents.1.content.2")}</li>
                            </ul>

                            <li>
                                <h1 className="font-bold text-xl">
                                    {t("services1.contents.2.title")}
                                </h1>
                                <ul class="list-disc">
                                    <li>
                                        {t("services1.contents.2.content.0")}
                                    </li>
                                    <li>
                                        {t("services1.contents.2.content.1")}
                                    </li>
                                    <li>
                                        {t("services1.contents.2.content.2")}
                                    </li>
                                </ul>

                                <li>
                                    <h1 className="font-bold text-xl">
                                        {t("services1.contents.3.title")}
                                    </h1>
                                    <ul class="list-disc">
                                        <li>
                                            {t(
                                                "services1.contents.3.content.0"
                                            )}
                                        </li>
                                        <li>
                                            {t(
                                                "services1.contents.3.content.1"
                                            )}
                                        </li>
                                        <li>
                                            {t(
                                                "services1.contents.3.content.2"
                                            )}
                                        </li>
                                    </ul>

                                    <li>
                                        <h1 className="font-bold text-xl">
                                            {t("services1.contents.4.title")}
                                        </h1>
                                        <ul class="list-disc">
                                            <li>
                                                {t(
                                                    "services1.contents.4.content.0"
                                                )}
                                            </li>
                                            <li>
                                                {t(
                                                    "services1.contents.4.content.1"
                                                )}
                                            </li>
                                            <li>
                                                {t(
                                                    "services1.contents.4.content.2"
                                                )}
                                            </li>
                                        </ul>
                                    </li>
                                </li>
                            </li>
                        </li>
                    </ul>
                </div>
            </Services>
        </>
    )
}
