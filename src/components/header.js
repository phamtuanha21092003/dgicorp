"use client"
import { useTranslations } from "next-intl"
import Image from "next/image"
import MenuIcon from "@mui/icons-material/Menu"
import React, { useEffect, useState } from "react"
import { Drawer } from "@mui/material"
import NavMenu from "./navmenu"
import { Link, useRouter, usePathname } from "@/navigation"
import BusinessIcon from "@mui/icons-material/Business"
import CallIcon from "@mui/icons-material/Call"
import EmailIcon from "@mui/icons-material/Email"

import Dropdow from "./dropdow"

const Header = () => {
    const t = useTranslations("Index")

    const router = useRouter()
    const pathname = usePathname()
    const [open, setOpen] = useState(false)
    const [isFixed, setIsFixed] = useState(false)
    const toggleDrawer = (newOpen) => () => {
        setOpen(newOpen)
    }

    const switchLanguage = (newLocale) => {
        const currentPath = pathname

        router.push(currentPath, { locale: newLocale })
    }

    useEffect(() => {
        const handleScroll = () => {
            if (window.scrollY > 200) {
                setIsFixed(true)
            } else {
                setIsFixed(false)
            }
        }

        window.addEventListener("scroll", handleScroll)

        return () => {
            window.removeEventListener("scroll", handleScroll)
        }
    }, [])

    return (
        <header
            className={`text-white w-full bg-white z-50 shadow transition-all duration-300 ease-in-out ${
                isFixed ? "fixed top-0 animate-stuckMoveDown" : ""
            }`}
        >
            <div
                className={`hidden px-2  text-black  ${
                    isFixed ? "hidden" : "md:flex md:flex-col items-center py-4"
                }   `}
            >
                <h2 className="text-4xl text-center font-bold">
                    {t("company_name")}
                </h2>
                <div className="flex flex-wrap gap-2 justify-between text-xl">
                    <div className="flex items-center gap-1">
                        <BusinessIcon
                            fontSize="large"
                            className="px-2 border bg-blue-500 text-white rounded-full"
                        />
                        {t("address_detail")}
                    </div>
                    <div className="flex items-center gap-1">
                        <CallIcon
                            fontSize="large"
                            className="px-2 border bg-blue-500 text-white rounded-full"
                        />
                        (028)37444804
                    </div>
                    <div className="flex items-center gap-1">
                        <EmailIcon
                            fontSize="large"
                            className="px-2 border bg-blue-500 text-white rounded-full"
                        />
                        info@dgicorp.vn
                    </div>
                </div>
            </div>
            <div className="bg-[#272727] py-2 w-full">
                <div className=" mx-auto h-full max-w-screen-xl grid grid-cols-12 justify-center items-center  ">
                    <div className="block ml-2 md:hidden">
                        <MenuIcon
                            onClick={toggleDrawer(true)}
                            className="cursor-pointer"
                        />
                    </div>
                    <div className="col-span-10 flex items-center justify-center w-full md:col-span-1">
                        <Image
                            src="/Logo.png"
                            width={60}
                            height={60}
                            alt="logo"
                        />
                    </div>
                    <div className="hidden md:col-span-10 md:flex ">
                        <div className="w-full flex justify-around">
                            <Link href="/">{t("menu_home")}</Link>
                            <Dropdow />
                            <Link href="/about">{t("menu_about")}</Link>
                            <Link href="/contact">{t("button_contact")}</Link>
                        </div>
                        <div className="flex gap-2 cursor-pointer items-center">
                            <div onClick={() => switchLanguage("vie")}>
                                <Image
                                    src="/vie.png"
                                    width={20}
                                    height={18}
                                    alt="vietnam"
                                />
                            </div>
                            <div onClick={() => switchLanguage("en")}>
                                <Image
                                    src="/america.png"
                                    width={20}
                                    height={18}
                                    alt="america"
                                />
                            </div>
                        </div>
                    </div>
                    <div></div>
                </div>
            </div>
            <Drawer
                sx={{
                    "& .MuiDrawer-paper": {
                        width: "80%",
                        boxSizing: "border-box",
                        backgroundColor: "black", // Change the background color
                        color: "white", // Change the text color
                    },
                }}
                open={open}
                onClose={toggleDrawer(false)}
            >
                <div className="p-2">
                    <div className="flex justify-center">
                        <Image
                            className="mr-6 md:mr-0"
                            src="/Logo.png"
                            width={60}
                            height={60}
                            alt="logo"
                        />
                    </div>
                    <hr className="my-2" />
                    <NavMenu
                        open={open}
                        setOpen={setOpen}
                        switchLanguage={switchLanguage}
                    />
                </div>
            </Drawer>
        </header>
    )
}

export default Header
