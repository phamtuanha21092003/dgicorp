"use client"
import { Link } from "@/navigation"
import { useTranslations } from "next-intl"
import React from "react"

export default function Banner() {
    const t = useTranslations("Index")
    return (
        <div className="w-[100vw] h-[350px] bg-[url('/banner4.jpg')] bg-center md:h-[600px]  relative text-white ">
            <div className=" p-5 bg-black bg-opacity-40  absolute text-sm bottom-10 md:bottom-1/3 md:left-20 text-wrap w-full  md:w-2/3 lg:w-1/2 animate__animated animate__backInDown">
                <div className="text-2xl font-bold ">{t("banner-content")}</div>
                <div className="flex gap-2 mt-4 font-semibold">
                    <Link
                        href="/contact"
                        className="px-4 py-2 bg-blue-500 border-blue-500 hover:text-blue-500 hover:bg-white  rounded animate__animated animate__backInLeft animate__delay-1s"
                    >
                        {t("button_contact")}
                    </Link>
                    <Link
                        href="/about"
                        className="px-4 py-2 border bg-white border-white text-blue-500 hover:bg-opacity-85 rounded animate__animated animate__backInLeft animate__delay-2s"
                    >
                        {t("button_more")}
                    </Link>
                </div>
            </div>
        </div>
    )
}
