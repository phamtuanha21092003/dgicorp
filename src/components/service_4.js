import Services from "@/components/services"
import { useTranslations } from "next-intl"
import React from "react"



export default function Service_4() {
    const t = useTranslations("Services")
    return (
        <>
            <Services title={t("services4.title")}>
                <div className="flex flex-col gap-3 px-2 ld:px-6">
                    <p class="indent-8 text-center ">
                        {t("services4.introduce")}
                    </p>
                    <ul className="list-decimal leading-loose px-2">
                        <li>
                            <h1 className="font-bold text-xl">
                                {t("services4.contents.0.title")}
                            </h1>
                            <ul class="list-disc">
                                <li>{t("services4.contents.0.content.0")}</li>
                                <li>{t("services4.contents.0.content.1")}</li>
                                <li>{t("services4.contents.0.content.2")}</li>
                            </ul>
                        </li>

                        <li>
                            <h1 className="font-bold text-xl">
                                {t("services4.contents.1.title")}
                            </h1>
                            <ul class="list-disc">
                                <li>{t("services4.contents.1.content.0")}</li>
                                <li>{t("services4.contents.1.content.1")}</li>
                                <li>{t("services4.contents.1.content.2")}</li>
                            </ul>
                        </li>

                        <li>
                            <h1 className="font-bold text-xl">
                                {t("services4.contents.2.title")}
                            </h1>
                            <ul class="list-disc">
                                <li>{t("services4.contents.2.content.0")}</li>
                                <li>{t("services4.contents.2.content.1")}</li>
                                <li>{t("services4.contents.2.content.2")}</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </Services>
        </>
    )
}
