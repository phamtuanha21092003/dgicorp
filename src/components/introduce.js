"use client"
import { Link } from "@/navigation"
import { useTranslations } from "next-intl"
import Image from "next/image"
import React from "react"

export default function Introduce() {
    const t = useTranslations("introduce")
    const t2 = useTranslations("Index")
    return (
        <div className="bg-blue-500 bg-opacity-5">
            <div className="max-w-screen-xl mx-auto py-5">
                <div className="flex flex-col md:grid md:grid-cols-12">
                    <div className="md:col-span-4 p-4">
                        <h1 className="text-3xl font-bold">{t("title")}</h1>
                        <p className="my-4 font-semibold">{t("content")}</p>
                        <Link
                            href="/about"
                            className="px-4 py-2 rounded border bg-blue-500 font-bold border-blue-500 text-white  hover:bg-blue-400 hover:border-blue-400 "
                        >
                            {t2("button_more")}
                        </Link>
                    </div>
                    <div className="col-span-8 p-4 flex flex-col gap-2">
                        <ABC
                            img="introduce_1.jpg"
                            content={t("content-1")}
                            reverse="reverse"
                        />
                        <ABC img="introduce_2.JPG" content={t("content-2")} />
                        <ABC
                            img="introduce_3.png"
                            content={t("content-3")}
                            reverse="reverse"
                        />
                        <ABC img="introduce_4.jpg" content={t("content-4")} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export const ABC = ({ reverse, img, content }) => {
    const imageStyle = {
        borderRadius: "100%",
    }
    return (
        <div
            className={`flex items-center animate__animated ${
                reverse ? "flex-row-reverse" : "flex-row"
            }`}
        >
            <div className="flex-1 p-2 font-medium">{content}</div>
            <div className="w-[124px] h-[124px] relative">
                <Image
                    src={`/${img}`}
                    sizes="100vw"
                    fill
                    style={imageStyle}
                    alt="banner"
                />
            </div>
        </div>
    )
}
