import dynamic from "next/dynamic"

const services = {
    service_1: dynamic(() => import("@/components/service_1")),
    service_2: dynamic(() => import("@/components/service_2")),
    service_3: dynamic(() => import("@/components/service_3")),
    service_4: dynamic(() => import("@/components/service_4")),
}

export default function Services_page({ slug }) {
    const ServiceComponent = services[slug]

    return (
        <div className="max-w-screen-xl mx-auto pb-6 px-2 md:px-4 lg:px-6">
            {ServiceComponent ? <ServiceComponent /> : <p>Service not found</p>}
        </div>
    )
}
