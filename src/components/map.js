"use client"
import { useTranslations } from "next-intl"
import React, { useState } from "react"
import CloseIcon from "@mui/icons-material/Close"
import WhereToVoteIcon from "@mui/icons-material/WhereToVote"

export default function Map() {
    const t = useTranslations("Index")
    const [open, setOpen] = useState(true)
    return (
        <div className="relative ">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.006766875244!2d106.7317846615304!3d10.810793089295618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752622c6cbb69b%3A0x84db8116a3ded60e!2zNDcgxJDGsOG7nW5nIFPhu5EgNCwgVGjhuqNvIMSQaeG7gW4sIFF14bqtbiAyLCBI4buTIENow60gTWluaCwgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1722408420732!5m2!1sen!2s"
                width="100%"
                height="600"
                style={{ border: 0 }}
                allowFullScreen
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
                className="mx-auto"
                sandbox="allow-scripts allow-same-origin"
            ></iframe>
            {!open && (
                <button
                    onClick={() => {
                        setOpen(!open)
                    }}
                    className="p-2 rounded-full border absolute bottom-[110px] right-2"
                >
                    <WhereToVoteIcon fontSize="large" />
                </button>
            )}
            {open && (
                <div className="absolute w-4/5 left-9 md:w-auto z-30 opacity-75 top-1/2 lg:top-1/2 lg:left-[30%] md:left-0">
                    <div className="bg-[#828483]  rounded p-4 text-xl font-bold text-white relative">
                        <div className="flex flex-col gap-2">
                            <h2>{t("company_name")}</h2>
                            <div className="text-sm">
                                <p>
                                    <span className="font-bold">
                                        {t("address")}
                                    </span>
                                    :{" "}
                                    <span className="">
                                        {t("address_detail")}
                                    </span>
                                </p>
                                <p>
                                    <span className="font-bold">
                                        {t("hotline")}
                                    </span>
                                    : <span className="">0903373701</span>
                                </p>
                                <p>
                                    <span className="font-bold">
                                        {t("phone")}:
                                    </span>{" "}
                                    <span className="">(028)37444804</span>
                                </p>
                                <p>
                                    <span className="font-bold">
                                        {" "}
                                        {t("email")}:{" "}
                                    </span>{" "}
                                    <span className="">info@dgicorp.vn</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <button
                        onClick={() => {
                            setOpen(!open)
                        }}
                        className="p-1 rounded-full border border-red-[#828483] bg-[#828483] hover:text-white absolute right-[-15px] top-[-15px]"
                    >
                        <CloseIcon fontSize="small" />
                    </button>
                </div>
            )}
        </div>
    )
}
