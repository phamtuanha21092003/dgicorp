"use client"
import React from "react"
import { useTranslations } from "use-intl"
import {
    EmailIcon,
    FacebookIcon,
    LinkedinIcon,
    PinterestIcon,
    XIcon,
} from "react-share"

export default function Fotter() {
    const t = useTranslations("Index")
    const currentYear = new Date().getFullYear()
    return (
        <div className="bg-[#f5f5f7] px-2 2xl:px-0 ">
            <div className="w-full md:max-w-screen-xl mx-auto py-4">
                <div className="flex flex-col gap-2">
                    <h1 className="font-bold uppercase  ">{t("contact")}</h1>
                    <h2>{t("company_name")}</h2>
                    <div className="text-sm">
                        <p>
                            <span className="font-bold">{t("address")}</span>:{" "}
                            <span className="text-gray-500">
                                {t("address_detail")}
                            </span>
                        </p>
                        <p>
                            <span className="font-bold">{t("hotline")}</span>:{" "}
                            <span className="text-gray-500">0903373701</span>
                        </p>
                        <p>
                            <span className="font-bold">{t("phone")}:</span>{" "}
                            <span className="text-gray-500">(028)37444804</span>
                        </p>
                        <p>
                            <span className="font-bold"> {t("email")}: </span>{" "}
                            <span className="text-gray-500">
                                info@dgicorp.vn
                            </span>
                        </p>
                    </div>
                    <div className="flex justify-end gap-2">
                        <FacebookIcon size={32} round={true} />
                        <XIcon size={32} round={true} />
                        <EmailIcon size={32} round={true} />
                        <PinterestIcon size={32} round={true} />
                        <LinkedinIcon size={32} round={true} />
                    </div>
                    <hr className="w-full bg-gray-300 my-4" />
                    <div>
                        <p className="text-gray-400 text-xs">
                            Copyright {currentYear} © Dgicorp. All rights
                            reserved.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}
