import { Inter } from "next/font/google"
import { AppRouterCacheProvider } from "@mui/material-nextjs/v13-appRouter"
import "./globals.css"
import "animate.css"
import { SpeedInsights } from "@vercel/speed-insights/next"

import { NextIntlClientProvider } from "next-intl"
import { getMessages, getLocale } from "next-intl/server"
import Header from "@/components/header"
import Fotter from "@/components/fotter"
import Head from "next/head"

const inter = Inter({ subsets: ["latin"] })

export default async function RootLayout({ children }) {
    const messages = await getMessages()
    const locale = await getLocale()
    return (
        <html lang={locale}>
            <Head>
                <link
                    rel="icon"
                    href="/favicon.ico"
                    type="image/x-icon"
                    sizes="16x16"
                />
            </Head>
            <body className={inter.className}>
                <AppRouterCacheProvider options={{ enableCssLayer: true }}>
                    <NextIntlClientProvider messages={messages}>
                        <Header />
                        <main className=" bg-gray-50  ">
                            {children}
                            <SpeedInsights />
                        </main>
                        <Fotter />
                    </NextIntlClientProvider>
                </AppRouterCacheProvider>
            </body>
        </html>
    )
}
