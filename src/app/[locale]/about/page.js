import { useTranslations } from "next-intl"
import Image from "next/image"
import React from "react"

export const metadata = {
    title: "DGICORP - About",
    description:
        "Nâng cấp đô thị, thành lập, chia tách, sáp nhập các đơn vị hành chính , cung các dịch vụ Đo đạc Bản đồ và Tài nguyên Môi trường.",
}

export default function Page() {
    const t = useTranslations("about")
    return (
        <div className="max-w-screen-xl mx-auto px-2 py-4 lg:px-0 flex flex-col gap-4 ">
            <div>
                <span className="font-bold text-xl">{t("name")}</span>
                <span>{t("content")}</span>
            </div>
            <div>{t("content2")}</div>
            <div>
                <p>{t("title")}</p>
                <ul className="list-disc ml-6 md:ml-8">
                    <li>{t("li.1")}</li>
                    <li>{t("li.2")}</li>
                    <li>{t("li.3")}</li>
                </ul>
            </div>
            <div>
                <p className="text-xl font-bold">{t("projects")}</p>
                <ul className="list-disc ml-6 md:ml-8">
                    <li>{t("li2.1")}</li>
                    <li>{t("li2.2")}</li>
                    <li>{t("li2.3")}</li>
                    <li>{t("li2.4")}</li>
                    <li>{t("li2.5")}</li>
                    <li>{t("li2.6")}</li>
                </ul>
            </div>
            <div>
                <p className="text-xl font-bold">{t("reward")}</p>
                <div className="flex flex-col md:flex-row gap-2">
                    <div className="w-full  p-2 flex flex-col justify-center items-center">
                        <Image
                            width={500}
                            height={300}
                            src="/bk_cn.jpg"
                            alt=""
                        />
                        <small className="mt-2">{t("li3.1")}</small>
                    </div>
                    <div className="w-full  p-2 flex flex-col justify-center items-center">
                        <Image
                            width={500}
                            height={300}
                            src="/bk_cty.jpg"
                            alt=""
                        />
                        <small className="mt-2">{t("li3.2")}</small>
                    </div>
                </div>
            </div>
            <div>
                <p className="text-xl font-bold">{t("management")}</p>
                <div className="w-full h-full bg-white flex justify-center p-3 mt-4">
                    <div className=" w-full h-[250px] md:h-[400px] object-contain ">
                        <img className="w-full h-full" src={t("img")} />
                    </div>
                </div>
            </div>
            <div className="flex justify-end">
                <Image width={120} height={120} src="/dg_logo.png" alt="logo" />
            </div>
        </div>
    )
}
