import Map from "@/components/map"
import React from "react"

export const metadata = {
    title: "DGICORP - Contact Information",
    description:
        "Nâng cấp đô thị, thành lập, chia tách, sáp nhập các đơn vị hành chính , cung các dịch vụ Đo đạc Bản đồ và Tài nguyên Môi trường.",
}

export default function Page() {
    return (
        <>
            <Map />
        </>
    )
}
