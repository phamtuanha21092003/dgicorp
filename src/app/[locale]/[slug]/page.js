import Services_page from "@/components/services_page"
import { getTranslations } from "next-intl/server"

const servicesMetadata = {
    service_1: {
        titleKey: "service_1.title",
        descriptionKey: "service_1.description",
    },
    service_2: {
        titleKey: "service_2.title",
        descriptionKey: "service_2.description",
    },
    service_3: {
        titleKey: "service_3.title",
        descriptionKey: "service_3.description",
    },
    service_4: {
        titleKey: "service_4.title",
        descriptionKey: "service_4.description",
    },
}

export async function generateMetadata({ params }) {
    const { slug, locale } = params
    const t = await getTranslations("Services")

    const metadata = servicesMetadata[slug] || {
        titleKey: "Services.default.title",
        descriptionKey: "Services.default.description",
    }

    return {
        title: t(metadata.titleKey),
        description: t(metadata.descriptionKey),
    }
}

export default function Page({ params }) {
    return <Services_page slug={params.slug} />
}
