import React from "react"

import Banner from "@/components/banner"
import Introduce from "@/components/introduce"
import Article from "@/components/article"

export const metadata = {
    title: "DGICORP - Home",
    description:
        "Nâng cấp đô thị, thành lập, chia tách, sáp nhập các đơn vị hành chính , cung các dịch vụ Đo đạc Bản đồ và Tài nguyên Môi trường.",
}

const Home = () => {
    return (
        <>
            <Banner />
            <Introduce />
            <Article />
        </>
    )
}

export default Home
