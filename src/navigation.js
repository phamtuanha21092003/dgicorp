import { createSharedPathnamesNavigation } from "next-intl/navigation"

const locales = ["en", "vie"]

export const { Link, redirect, usePathname, useRouter } =
    createSharedPathnamesNavigation({ locales })
